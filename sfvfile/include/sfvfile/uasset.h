//
// Created by WydD on 07/07/2018.
//

#ifndef SFVSIM_NATIVE_UASSET_H
#define SFVSIM_NATIVE_UASSET_H

#include "json.hpp"

using json = nlohmann::json;

class Uasset {
public:
    json data;
    std::vector<std::vector<uint8_t>> binary_data;

    std::array<uint8_t, 24> header{};
    std::array<uint8_t, 4> uk1{};
    std::array<uint8_t, 4> uk2{};
    std::array<uint8_t, 16> uk_checksum{};
    std::array<uint8_t, 4> footer{};
    bool fully_parsed{true};

    Uasset(): data(json::object()) {}

    friend std::istream& operator>>(std::istream& is, Uasset& obj);
};


#endif //SFVSIM_NATIVE_UASSET_H
