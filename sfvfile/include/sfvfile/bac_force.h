//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_FORCE_H
#define SFVSIM_NATIVE_BAC_FORCE_H

#include "bac_time_element.h"

class Force: public TimeElement {
public:
    template <typename TF>
    Force(int32_t tick_start, int32_t tick_end, TF &tf, float amount, uint32_t flags) :
            TimeElement(tick_start, tick_end, tf), amount(amount), flags(flags) {
        if (flags == 0) {
            type = 0;
            type_flag = 0;
        } else if (flags & 0xf) {
            type = 1;
            type_flag = static_cast<uint8_t>(flags);
        } else if (flags & 0xf0) {
            type = 2;
            type_flag = static_cast<uint8_t>(flags >> 4);
        } else if (flags & 0xf00) {
            type = 3;
            type_flag = static_cast<uint8_t>(flags >> 8);
        } else if (flags & 0xf000) {
            type = 4;
            type_flag = static_cast<uint8_t>(flags >> 12);
        } else if (flags & 0xf0000) {
            type = 5;
            type_flag = static_cast<uint8_t>(flags >> 16);
        } else {
            type = 6;
            type_flag = static_cast<uint8_t>(flags >> 20);
        }

    }
    float amount;
    uint32_t flags;
    uint8_t type;
    uint8_t type_flag;
};


#endif //SFVSIM_NATIVE_BAC_FORCE_H
