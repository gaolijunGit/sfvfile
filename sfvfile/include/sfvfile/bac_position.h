//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_POSITION_H
#define SFVSIM_NATIVE_BAC_POSITION_H

#include <cstdint>
#include <optional>
#include <vector>
#include <cmath>
#include "util.h"

class Position : private noncopyable {
public:
    Position(int32_t time) : time(0), values(time, NAN) {}

    int16_t time;
    std::vector<float> values;
    // structure is [active_on_states << 32 | inactive_on_states, position]
    std::unordered_map<uint64_t, std::unique_ptr<Position>> conditional_positions;

    std::optional<float> at(int16_t desired_time) {
        auto index = desired_time - time;
        if (index < 0 || static_cast<uint16_t>(index) >= values.size()) {
            return {};
        }
        float result = values[index];
        if (std::isnan(result)) {
            return {};
        } else {
            return result;
        }
    }
    std::optional<float> at(int32_t state, int16_t desired_time) {
        std::optional<float> result = at(desired_time);
        for (auto &it: conditional_positions) {
            auto pos = it.second.get();
            auto &condition = it.first;
            int32_t active_on_states = (condition >> 32);
            if (active_on_states > 0 && !(active_on_states & state)) {
                continue;
            }
            int32_t inactive_on_states = (condition & 0xFFFFFFFF);
            if (inactive_on_states > 0 && (inactive_on_states & state)) {
                continue;
            }
            auto conditional_value = pos->at(desired_time);
            if (!result) {
                result = conditional_value;
                continue;
            }
            if (!conditional_value) {
                continue;
            }
            *result += *conditional_value;
        }
        return result;
    }
};

#endif //SFVSIM_NATIVE_BAC_POSITION_H
