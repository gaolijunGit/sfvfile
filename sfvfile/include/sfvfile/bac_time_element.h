//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_TIME_ELEMENT_H
#define SFVSIM_NATIVE_BAC_TIME_ELEMENT_H

#include <cstdint>
#include <list>
#include "util.h"

class TimeElement: private noncopyable {
public:
    template<typename TF>
    TimeElement(int32_t tick_start, int32_t tick_end, TF &tick_flags) :
            tick_start(tick_start), tick_end(tick_end),
            element_active_on_states(tick_flags.element_active_on_states),
            element_inactive_on_states(tick_flags.element_inactive_on_states),
            element_unknown3(tick_flags.element_unknown3), element_unknown4(tick_flags.element_unknown4) {}

    int32_t tick_start;
    int32_t tick_end;
    int32_t element_active_on_states;
    int32_t element_inactive_on_states;
    int32_t element_unknown3;
    int32_t element_unknown4;
};

template<class TimeElementType>
inline std::list<TimeElementType*> timeline_at(std::vector<TimeElementType> &container, int32_t time, int32_t state) {
    std::list<TimeElementType*> result{};
    for(auto &it : container) {
        if (it.tick_start <= time && time < it.tick_end) {
            if (it.element_active_on_states) {
                if (!(it.element_active_on_states & state)) {
                    continue;
                }
            }
            if (it.element_inactive_on_states) {
                if (it.element_inactive_on_states & state) {
                    continue;
                }
            }
            result.push_back(static_cast<TimeElementType*>(&it));
        }
    }
    return result;
}


template<class TimeElementType>
inline std::list<std::unique_ptr<TimeElementType>> timeline_clone_at(std::vector<TimeElementType> &container, int32_t time, int32_t state, float dx, float dy, bool side) {
    std::list<std::unique_ptr<TimeElementType>> result{};
    for(auto &it : container) {
        if (it.tick_start <= time && time < it.tick_end) {
            if (it.element_active_on_states) {
                if (!(it.element_active_on_states & state)) {
                    continue;
                }
            }
            if (it.element_inactive_on_states) {
                if (it.element_inactive_on_states & state) {
                    continue;
                }
            }
            result.push_back(it.clone(dx, dy, side));
        }
    }
    return result;
}

#endif //SFVSIM_NATIVE_BAC_TIME_ELEMENT_H
