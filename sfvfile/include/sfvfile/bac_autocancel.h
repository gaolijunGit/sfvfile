//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_AUTOCANCEL_H
#define SFVSIM_NATIVE_BAC_AUTOCANCEL_H

#include <vector>
#include "bac_time_element.h"
#include "util.h"

class AutoCancel : public TimeElement {
public:
    template<typename Tick, typename TF, typename Elt>
    AutoCancel(Tick &tick, TF &tf, Elt &elt, uint32_t number_of_parameters) :
            TimeElement(tick.tick_start, tick.tick_end, tf), condition(elt.condition), script_index(elt.script_index),
            script_time(elt.script_time), execution_flag(elt.execution_flag), state_change(elt.state_change),
            unknown3(elt.unknown3), unknown4(elt.unknown4), parameters(number_of_parameters) {}

    uint8_t condition;
    int16_t script_index;
    int16_t script_time;
    uint32_t execution_flag;

    uint8_t state_change;
    uint32_t unknown3;
    uint32_t unknown4;

    std::vector<int32_t> parameters;
};

#endif //SFVSIM_NATIVE_BAC_AUTOCANCEL_H
