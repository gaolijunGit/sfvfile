//
// Created by WydD on 31/03/2018.
//

#ifndef SFVSIM_NATIVE_BCH_H
#define SFVSIM_NATIVE_BCH_H


#include <string>
#include <map>
#include "bcheader.h"

class BCH {
private:
    void init();
public:
    int32_t life;
    int32_t stun;
    int32_t vgauge;
    int32_t vgauge_2;
    int32_t vtrigger;
    int32_t vtrigger_2;
    int32_t claw_endurance;
    int32_t mask_endurance;
    int32_t vtrigger_move_speed;
    std::map<std::string, uint32_t> content;
    friend std::istream& operator>>(std::istream& is, BCH& obj);
    void load_data(uint8_t* data);
    BCHeader header;
};


#endif //SFVSIM_NATIVE_BCH_H
