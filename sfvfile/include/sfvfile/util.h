//
// Created by WydD on 07/04/2018.
//

#ifndef SFVSIM_NATIVE_UTIL_H
#define SFVSIM_NATIVE_UTIL_H

#include <cstdint>
#include <string>
#include <exception>
#include <stdexcept>

//#define DEBUG_LOG
#ifdef DEBUG_LOG
#define LOG(...) printf(__VA_ARGS__);
#else
#define LOG(...)
#endif

class noncopyable {
public:
    noncopyable() = default;
    noncopyable(const noncopyable&) = delete;
    noncopyable& operator = (const noncopyable&) = delete;

    noncopyable(noncopyable&&) = default;
    noncopyable& operator = (noncopyable&&) = default;
};

class ParseError : public std::runtime_error {
public:
    explicit ParseError(const std::string &message) : runtime_error(message) {};
};


#endif //SFVSIM_NATIVE_UTIL_H
