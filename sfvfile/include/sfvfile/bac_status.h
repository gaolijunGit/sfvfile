//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_STATUS_H
#define SFVSIM_NATIVE_BAC_STATUS_H

#include "bac_time_element.h"

class Status: public TimeElement {
public:
    template<typename TF>
    Status(int32_t tick_start, int32_t tick_end, TF &tf, uint32_t player_flags, uint32_t opponent_flags) :
            TimeElement(tick_start, tick_end, tf), player_flags(player_flags), opponent_flags(opponent_flags) {}
    uint32_t player_flags;
    uint32_t opponent_flags;
};

#endif //SFVSIM_NATIVE_BAC_STATUS_H
