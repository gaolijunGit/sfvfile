//
// Created by WydD on 31/03/2018.
//

#include <iostream>
#include <cstring>
#include <memory>
#include "sfvfile/bch.h"
#include "util/ioutil.h"


std::istream &operator>>(std::istream &is, BCH &obj) {
    // load the uasset file
    std::vector<uint8_t> content = read_uasset(is);
    uint8_t* base = content.data();
    obj.load_data(base);
    return is;
}

void BCH::load_data(uint8_t *base) {
    uint8_t* ptr = base;

    // read the bch header
    header = *read_and_advance<BCHeader>(ptr);
    check_header(header, "#BCH");

    // read the key-value map
    // list structure with
    struct kv {
        char key[0x20];
        uint32_t value;
    };
    auto iterable = relative_iterable<kv>(base, ptr, ptr + 4);
    for (kv* ref : iterable) {
        content[ref->key] = ref->value;
    }
    init();
}

template <typename V, typename R>
R get_default(const  std::map <std::string,V> & m, const std::string & key, const R & defval ) {
    typename std::map<std::string,V>::const_iterator it = m.find( key );
    if ( it == m.end() ) {
        return defval;
    }
    else {
        return static_cast<R>(it->second);
    }
}

void BCH::init() {
    life = content.at("VitalGauge");
    stun = content.at("StunGauge");
    const int32_t default_vgauge = get_default(content, "VGauge", -1);
    vgauge = get_default(content, "VGauge0", default_vgauge);
    vgauge_2 = get_default(content, "VGauge1", default_vgauge);
    const int32_t default_vtime = get_default(content, "VT1_Time", -1);
    vtrigger = get_default(content, "VT_Time0", default_vtime);
    vtrigger_2 = get_default(content, "VT_Time1", default_vtime);
    claw_endurance = get_default(content, "ClawEndurance", -1);
    mask_endurance = get_default(content, "MaskEndurance", -1);
    vtrigger_move_speed = get_default(content, "VT1_MoveSpeed", -1);
}
