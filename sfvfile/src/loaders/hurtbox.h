//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_HURTBOX_H
#define SFVSIM_NATIVE_LOADER_HURTBOX_H

#include "box.h"
#include "../../include/sfvfile/bac.h"

struct hurtbox_base : box_base {
    int16_t vulnerability_flags;
    int16_t unknown7;
    int8_t armor_group;
    int8_t armor_strength;
    int8_t armor_count;
    uint8_t unknown9;

    uint32_t hurt_type;

    int16_t armor_effect;
    int16_t hurt_level;
    int16_t armor_freeze;
    int16_t unknown11;

    float unknown12;
};

struct hurtbox_ver1 : hurtbox_base {
    uint32_t unknown13;
};
struct hurtbox_ver0 : hurtbox_base {
    static const uint32_t unknown13;
};
const uint32_t hurtbox_ver0::unknown13 = 0;

template<typename T>
inline void load_hurtbox(Script &script, tick_header &tick_header, tick_flags& tf, T &elt) {
    script.hurtboxes.emplace_back(tick_header, tf, elt);
}

#endif //SFVSIM_NATIVE_LOADER_HURTBOX_H
