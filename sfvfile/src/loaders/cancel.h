//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_CANCEL_H
#define SFVSIM_NATIVE_LOADER_CANCEL_H

#include "../../include/sfvfile/bac.h"

struct cancel_struct {
    int32_t cancel_list;
    uint32_t type;
};
inline void load_cancel(Script &script, tick_header& tick, tick_flags& tf, cancel_struct& elt) {
    script.cancels.emplace_back(tick.tick_start, tick.tick_end, tf, elt.cancel_list, elt.type);
}

#endif //SFVSIM_NATIVE_LOADER_CANCEL_H
