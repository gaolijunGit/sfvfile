//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_FORCE_H
#define SFVSIM_NATIVE_LOADER_FORCE_H

#include "../../include/sfvfile/bac.h"

struct force_struct {
    float amount;
    uint32_t flags;
};

inline void load_force(Script &script, tick_header& tick, tick_flags& tf, force_struct& elt) {
    script.forces.emplace_back(tick.tick_start, tick.tick_end, tf, elt.amount, elt.flags);
}


#endif //SFVSIM_NATIVE_LOADER_FORCE_H
