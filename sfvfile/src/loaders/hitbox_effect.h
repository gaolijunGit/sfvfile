//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_HITBOX_EFFECT_H
#define SFVSIM_NATIVE_LOADER_HITBOX_EFFECT_H

#include <cstdint>

struct hitbox_effect_base {
    int16_t type;
    int16_t index;
    int32_t damage_type;
    uint8_t unused1;
    uint8_t number_of_type1;
    uint8_t number_of_type2;
    uint8_t unused2;
    int16_t damage;
    int16_t stun;
    int16_t index9;
    int16_t vtimer;
    int16_t ex_attacker;
    int16_t ex_defender;
    int32_t vgauge;
    int16_t hit_freeze_attacker;
    int16_t index14;
    int16_t hit_freeze_defender;
    int16_t fuzzy_effect;
    int16_t main_recovery_duration;
    int16_t knockdown_duration;
    int8_t juggle_start;
    int8_t index18;
    int16_t damage_scaling_group;
    float knock_back;
    float fall_speed;
    int32_t index22;
    int32_t index23;
    int32_t index24;
    int32_t index25;
    int32_t type1_offset_ptr;
    int32_t type2_offset_ptr;
};

#endif //SFVSIM_NATIVE_LOADER_HITBOX_EFFECT_H
