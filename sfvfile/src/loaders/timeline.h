//
// Created by WydD on 11/08/2018.
//

#ifndef SFVSIM_NATIVE_TIMELINE_H
#define SFVSIM_NATIVE_TIMELINE_H


struct tick_header {
    int32_t tick_start;
    int32_t tick_end;
};

struct tick_flags {
    int32_t element_active_on_states;
    int32_t element_inactive_on_states;
    int32_t element_unknown3;
    int32_t element_unknown4;
};


#endif //SFVSIM_NATIVE_TIMELINE_H
