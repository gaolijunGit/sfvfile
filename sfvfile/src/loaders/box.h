//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_BOX_H
#define SFVSIM_NATIVE_LOADER_BOX_H

#include <cstdint>

struct box_base {
    float x;
    float y;
    float z;
    float width;
    float height;

    uint32_t unknown1;
    uint16_t unknown2;
    uint16_t unknown3;
    uint16_t unknown4;
    uint16_t unknown5;
};

#endif //SFVSIM_NATIVE_LOADER_BOX_H
