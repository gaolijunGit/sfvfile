//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_AUTO_CANCEL_H
#define SFVSIM_NATIVE_LOADER_AUTO_CANCEL_H

#include <functional>
#include "../../include/sfvfile/bac.h"
#include <cstring>

struct auto_cancel_base {
    uint8_t condition;
    uint8_t state_change;
    int16_t script_index;
    int16_t script_time;
    uint16_t number_of_parameters;
    uint32_t execution_flag;
};

struct auto_cancel_ver0: auto_cancel_base {
    uint32_t parameter_ptr;
    static const char unknown3;
    static const char unknown4;
};
const char auto_cancel_ver0::unknown3 = 0;
const char auto_cancel_ver0::unknown4 = 0;

struct auto_cancel_ver1: auto_cancel_base {
    uint32_t unknown3;
    uint32_t unknown4;
    uint32_t parameter_ptr;
};

template<typename T>
void load_auto_cancel(Script &script, tick_header& tick, tick_flags& tf, T& elt) {
    auto &auto_cancel = script.auto_cancels.emplace_back(tick, tf, elt, elt.number_of_parameters);
    if (elt.number_of_parameters > 0) {
        memcpy(auto_cancel.parameters.data(), ((uint8_t*) &elt)+elt.parameter_ptr, elt.number_of_parameters * sizeof(uint32_t));
    }
}

#endif //SFVSIM_NATIVE_LOADER_AUTO_CANCEL_H
