//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_PUSHBOX_H
#define SFVSIM_NATIVE_LOADER_PUSHBOX_H

#include "box.h"
#include "../../include/sfvfile/bac.h"

struct pushbox_base : box_base {
    uint32_t unknown6;
};

inline void load_pushbox(Script &script, tick_header &tick_header, tick_flags& tf, pushbox_base &elt) {
    script.pushboxes.emplace_back(tick_header, tf, elt);
}

#endif //SFVSIM_NATIVE_LOADER_PUSHBOX_H
