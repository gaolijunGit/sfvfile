//
// Created by WydD on 07/07/2018.
//

#include <sfvfile/util.h>
#include "sfvfile/uasset.h"


class ParseException: std::exception {

};

json read_array(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset &obj);

json read_struct(std::istream& ptr, std::vector<std::string>& all_strings, int& none_idx, json& declare, Uasset &obj);

json read_string(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset &obj);

json read_int(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset &obj);

json read_float(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset &obj);

json read_object(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset &obj);

json read_byte(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset &obj);

json read_text(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset &obj);

json read_name(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset &obj);

json read_bool(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset &obj);

json read_void(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset& obj) {
    return {};
}

std::string read_string_from_stream(std::istream &ptr);

json read_array_content(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx,
                        json &declare,
                        json (*property_type)(std::istream &, std::vector<std::string> &, int &, json &, Uasset&),
                        Uasset& obj
);


inline void read_zero(std::istream &file) {
    long long ptr = file.tellg();
    uint32_t zero;
    file.read((char*) &zero, sizeof(zero));
    if (zero != 0) {
        LOG("I'm completely lost! Expecting zero but got 0x%x at 0x%x\n", zero, ptr);
        throw ParseException();
    }
}

inline void read_zeros(std::istream &file, int n) {
    uint8_t zero;
    for (int i = 0 ; i < n ; ++i) {
        long long ptr = file.tellg();
        file.read((char*) &zero, 1);
        if (zero != 0) {
            LOG("Unexpected! Expecting zero but got 0x%x at 0x%x\n", zero, ptr);
            throw ParseException();
        }
    }
}

auto read_property_type(std::istream& ptr, std::vector<std::string>& all_strings) {
    int32_t buf;
    ptr.read((char *) &buf, sizeof(int32_t));
    read_zero(ptr);
    std::string property_type = all_strings[buf];
    LOG("\tRead property type %s [%d, %x]\n", property_type.c_str(), buf, buf);
    if (property_type == "StructProperty") {
        return read_struct;
    }
    if (property_type == "StrProperty") {
        return read_string;
    }
    if (property_type == "IntProperty") {
        return read_int;
    }
    if (property_type == "FloatProperty") {
        return read_float;
    }
    if (property_type == "ArrayProperty") {
        return read_array;
    }
    if (property_type == "ObjectProperty") {
        return read_object;
    }
    if (property_type == "ByteProperty") {
        return read_byte;
    }
    if (property_type == "TextProperty") {
        return read_text;
    }
    if (property_type == "NameProperty") {
        return read_name;
    }
    if (property_type == "BoolProperty") {
        return read_bool;
    }
    LOG("Unknown property %d %x, %s\n", buf, buf, property_type.c_str());
    return read_void;
}


json read_struct(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    LOG("Read struct\n");
    json result = json::object();
    while(true) {
        int32_t buf, size;
        ptr.read((char *) &buf, sizeof(int32_t));
        read_zero(ptr);
        if (buf == none_idx) {
            LOG("\tBreaking (encountered none %d %x)\n", none_idx, none_idx);
            break;
        }

        auto reference = all_strings[buf];
        if (reference == "Vector" || reference == "Rotator") {
            result["_class"] = reference;
            float x, y, z;
            ptr.read((char *) &x, sizeof(x));
            result["x"] = x;
            ptr.read((char *) &y, sizeof(y));
            result["y"] = y;
            ptr.read((char *) &z, sizeof(z));
            result["z"] = z;
            LOG("\tread %s x, y, z: (%f, %f, %f)\n", reference.c_str(), x, y, z);
            return result;
        }
        if (reference == "IntPoint") {
            result["_class"] = reference;
            int32_t x, y;
            ptr.read((char *) &x, sizeof(x));
            result["x"] = x;
            ptr.read((char *) &y, sizeof(y));
            result["y"] = y;
            LOG("\tread %s x, y: (%d, %d)\n", reference.c_str(), x, y);
            return result;
        }
        if (reference == "Guid") {
            result["_class"] = reference;
            uint8_t data[16];
            ptr.read((char *) &data, sizeof(data));
            std::stringstream ss;
            ss << std::hex;
            for(int i=0;i<16;++i)
                ss << std::setw(2) << std::setfill('0') << (int)data[i];
            auto str = ss.str();
            result["value"] = str;
            LOG("\tread %s %s\n", reference.c_str(), str.c_str());
            return result;
        }
        LOG("\tattribute: %s [%d, %x]\n", reference.c_str(), buf, buf);
        if (reference == "StringAssetReference") {
            result[reference] = read_string(ptr, all_strings, none_idx, declare, obj);
            return result;
        }
        auto property_type = read_property_type(ptr, all_strings);
        if (property_type == read_void && result.empty()) {
            ptr.seekg(-8+ptr.tellg());
            // this seems to be a type
            result["_class"] = reference;
            LOG("\trollback! -> actually structure type is: %s [%d, %x]\n", reference.c_str(), buf, buf);
            ptr.read((char *) &buf, sizeof(int32_t));
            read_zero(ptr);
            reference = all_strings[buf];
            LOG("\tattribute: %s [%d, %x]\n", reference.c_str(), buf, buf);
            property_type = read_property_type(ptr, all_strings);
        }
        if (property_type == read_void) {
            long long fpos = ptr.tellg();
            LOG("I expected a good property type at 0x%x but got nothing good, quitting!\n", fpos);
            throw ParseException();
        }
        ptr.read((char *) &size, sizeof(int32_t));
        read_zero(ptr);
        if (property_type == read_byte) {
            size *= 2;
        }
        LOG("\t\tsize: %d\n", size);
//        auto fpos = ptr.tellg();
        result[reference] = property_type(ptr, all_strings, none_idx, declare, obj);
//        ptr.seekg(size+fpos);
    }
    return result;
}

json read_string(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    std::string result = read_string_from_stream(ptr);
    return result;
}

std::string read_string_from_stream(std::istream &ptr) {
    uint32_t size;
    ptr.read((char*) &size, sizeof(uint32_t));
    if (size == 0) {
        return "";
    }
    auto *content = new char[size];
    ptr.read(content, size);
    std::string result = std::string(content, size - 1);
    LOG("Read string: %s, size: %d\n", result.c_str(), size);
    return result;
}

json read_int(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    int32_t result;
    ptr.read((char*) &result, sizeof(uint32_t));
    LOG("Read int: %d\n", result);
    return result;
}

json read_float(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    float result;
    ptr.read((char*) &result, sizeof(result));
    LOG("Read float: %f\n", result);
    return result;
}

json read_bool(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    bool result;
    ptr.read((char*) &result, sizeof(result));
    LOG("Read float: %d\n", result);
    return result;
}

json read_object(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    int32_t result;
    ptr.read((char*) &result, sizeof(uint32_t));
    if (result == 0) {
        LOG("Read null object\n");
        json res = {{"_type", "object"}, {"_id", -1}, {"name", nullptr}};
        return res;
    }
    LOG("Read object %d", result);
    if (result > 0) {
        LOG(": unknown object!\n");
        json res = {{"_type", "object"}, {"_id", result}, {"name", nullptr}};
        return res;
    }
    auto reference = declare[-result - 1];
    LOG(" %s\n", reference["name"].get<std::string>().c_str());
    json res = {{"_type", "object"}, {"_id", -result - 1}, {"name", reference["name"]}};
    return res;
}

json read_byte(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset& obj) {
    uint32_t category, value;
    ptr.read((char*) &category, sizeof(uint32_t));
    read_zero(ptr);
    ptr.read((char*) &value, sizeof(uint32_t));
    read_zero(ptr);
    LOG("Read byte %s[%d %x] %s[%d %x]\n", all_strings[category].c_str(), category, category, all_strings[value].c_str(), value, value);
    json res = {{"_type", "enum"}, {"category", all_strings[category]}, {"value", all_strings[value]}};
    return res;
}

json read_text(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json &declare, Uasset& obj) {
    json result = json::object();
    result["_type"] = "text";
    json extracted = json::array();
    // 1 + 4 + 4 each time
    read_zero(ptr);
    int8_t next;
    ptr.read((char*) &next, sizeof(next));
    if (next == 0) {
        read_zero(ptr);
        result["uuid"] = read_string_from_stream(ptr);
        result["content"] = read_string_from_stream(ptr);
    } else {
        result["id"] = next;
    }
    return result;
}

json read_name(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    uint32_t name_id;
    ptr.read((char *) &name_id, sizeof(name_id));
    read_zero(ptr);
    if (none_idx == name_id) {
        return {};
    }
    return all_strings[name_id];
}

json read_array(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx, json& declare, Uasset& obj) {
    LOG("Read array\n");
    auto property_type = read_property_type(ptr, all_strings);
    return read_array_content(ptr, all_strings, none_idx, declare, property_type, obj);
}

json read_array_content(std::istream &ptr, std::vector<std::string> &all_strings, int &none_idx,
                        json &declare,
                        json (*property_type)(std::istream &, std::vector<std::string> &, int &, json &, Uasset&),
                        Uasset& obj) {
    uint32_t size;
    ptr.read((char*) &size, sizeof(uint32_t));
    if (property_type == read_byte) {
        // special case for array[byte] = binary data
        LOG("\tbyte array special case\n");

        std::vector<uint8_t> content(size);
        ptr.read((char*)(content.data()), size);
        json res = {{"_type", "bytes"}, {"value", obj.binary_data.size()}, {"size", size}};
        obj.binary_data.push_back(std::move(content));
        return res;
    }
    LOG("\tsize %d\n", size);
    json result = json::array();
    for (auto i = 0 ; i < size ; i++) {
        result.push_back(property_type(ptr, all_strings, none_idx, declare, obj));
    }
    return result;
}



std::istream &operator>>(std::istream &file, Uasset &obj) {

    file.read((char*) obj.header.data(), sizeof(obj.header));

    int32_t header_size;
    file.read((char*) &header_size, sizeof(uint32_t));
    std::string none_str;
    int none_idx = -1;
    {
        uint32_t size;
        file.read((char*) &size, sizeof(uint32_t));
        auto *content = new char[size];
        file.read(content, size);
        none_str = {content, size-1};
    }

    long long none_ptr = file.tellg();

    file.read((char*) obj.uk1.data(), sizeof(obj.uk1));
    std::vector<std::string> all_strs = {};

    int32_t lst_size, lst_ptr;
    file.read((char*) &lst_size, sizeof(uint32_t));
    file.read((char*) &lst_ptr, sizeof(uint32_t));
    file.seekg(lst_ptr);
    for(auto i = 0 ; i < lst_size ; i++) {
        uint32_t size;
        file.read((char*) &size, sizeof(uint32_t));
        auto *content = new char[size];
        file.read(content, size);
        std::string ref = all_strs.emplace_back(content, size-1);
        if (ref == none_str) {
            none_idx = i;
        }
        LOG("0x%x\t%s\n", i, content);
        delete[] content;
    }

    file.seekg(none_ptr+0x14);
    file.read((char*) &lst_size, sizeof(uint32_t));
    file.read((char*) &lst_ptr, sizeof(uint32_t));
    file.seekg(lst_ptr);
    auto declare = json::array();
    for(auto i = 0 ; i < lst_size ; i++) {
        int32_t t[7];
        file.read((char*) &t, sizeof(t));
        LOG("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%s\n\t%s / type: %s\n", t[0], t[1], t[2], t[3], t[4], t[5], t[6], all_strs[t[0]].c_str(), all_strs[t[5]].c_str(), all_strs[t[2]].c_str());
        json content = {
                {"id",        i},
                {"namespace", all_strs[t[0]]},
                {"type",      all_strs[t[2]]},
                {"name",      all_strs[t[5]]},
        };
        if (t[4] != 0) {
            content["depends"] = -t[4] - 1;
        }
        declare.push_back(content);
    }

    LOG("Finishing declare block\n");
    LOG(declare.dump(2).c_str());
    LOG("\n");
    obj.data["declare"] = std::move(declare);
//        LOG("\n---\n");
    file.seekg(none_ptr+0xC);
    file.read((char*) &lst_size, sizeof(uint32_t));
    file.read((char*) &lst_ptr, sizeof(uint32_t));
    file.seekg(lst_ptr);
    auto content = json::array();
    for(auto i = 0 ; i < lst_size ; i++) {
        int32_t t[17];
        file.read((char*) &t, sizeof(t));
        LOG("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n%s\n", t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], t[8], t[9], t[10], t[11], t[12], t[13], t[14], t[15], t[16], all_strs[t[3]].c_str());
        json reference;
        if (t[0] >= 0) {
            reference = t[0];
        } else {
            reference = declare[-t[0] - 1]["name"];
        }

        json res = {
                {"id",    i},
                {"class", reference},
                {"name",  all_strs[t[3]]},
        };
        long long fpos = file.tellg();
        LOG("%lld\n", fpos);
        file.seekg(t[7]);
        try {
            res["value"] = read_struct(file, all_strs, none_idx, declare, obj);
        } catch (ParseException&) {
            file.seekg(fpos);
            content.push_back(std::move(res));
            obj.fully_parsed = false;
            continue;
        }
        read_zero(file);
        long long read_bytes = file.tellg();
        LOG("ptr %lld\n", read_bytes);
        read_bytes -= t[7];
        if (read_bytes < t[6]) {
            LOG("FOUND FILE %lld %d\n", read_bytes, t[6]);
//                file.read((char*) &uk_ptr, sizeof(uint32_t));
//                file.read((char*) &uk_ptr, sizeof(uint32_t));
            try {
                json additional = read_array_content(file, all_strs, none_idx, declare, read_struct, obj);
                res["misc"] = additional;
            } catch (ParseException&) {
                file.seekg(fpos);
                obj.fully_parsed = false;
            }
        } else if (read_bytes != t[6]) {
            LOG("ERROR, WE ARE NOT AT THE END OF THE BLOCK! %lld %d\n", read_bytes, t[6]);
            //exit(5);
        }
        file.seekg(fpos);
        content.push_back(std::move(res));
    }

    obj.data["content"] = content;

    file.seekg(none_ptr+0x1C);
    file.read((char*) &lst_ptr, sizeof(uint32_t));
    file.seekg(lst_ptr);
    file.read((char*) &lst_size, sizeof(uint32_t));
    auto import = json::array();
    for(auto i = 0 ; i < lst_size ; i++) {
        int32_t obj;
        file.read((char*) &obj, sizeof(uint32_t));
        LOG("%d ", obj);
        obj = -obj-1;
        if (obj > 0) {
            json obj_json = {
                    {"id",   obj},
                    {"name", declare[obj]["name"]}
            };
            import.push_back(obj_json);
        } else {
            json obj_json = {
                    {"id",   obj}
            };
            import.push_back(obj_json);
        }
    }
    LOG("\n");
    obj.data["import"] = std::move(import);

    file.seekg(none_ptr+0x20);
    file.read((char*) &lst_size, sizeof(uint32_t));
    file.read((char*) &lst_ptr, sizeof(uint32_t));
    file.seekg(lst_ptr);
    auto load = json::array();
    for(auto i = 0 ; i < lst_size ; i++) {
        uint32_t size;
        file.read((char*) &size, sizeof(uint32_t));
        auto *content = new char[size];
        file.read(content, size);
        load.push_back(std::string(content, size-1));
        LOG("%s\n", content);
        delete[] content;
    }
    LOG("\n");
    obj.data["uk_depends"] = std::move(load);

    file.seekg(none_ptr+0x28);
    try {
        read_zero(file);
    } catch (ParseException&) {
        obj.fully_parsed = false;
        return file;
    }

    file.read((char*)obj.uk_checksum.data(), sizeof(obj.uk_checksum));

    int32_t uk_int;
    file.read((char*)&uk_int, sizeof(uk_int));
    if (uk_int != 1) {
        LOG("Expected to read 1 after the checksum... actual value 0x%x", uk_int);
        obj.fully_parsed = false;
        return file;
    }
    file.read((char*)&uk_int, sizeof(uk_int));
    if (uk_int != obj.data["content"].size()) {
        LOG("Expected to read the length of 'content' 0x%x found 0x%x", obj.data["content"].size(), uk_int);
        obj.fully_parsed = false;
        return file;
    }
    file.read((char*)&uk_int, sizeof(uk_int));
    if (uk_int != all_strs.size()) {
        LOG("Expected to read the numbers of the strings again 0x%x found 0x%x", all_strs.size(), uk_int);
        obj.fully_parsed = false;
        return file;
    }
    try {
        read_zeros(file, 0x16);
    } catch (ParseException&) {
        obj.fully_parsed = false;
        return file;
    }

    file.read((char*)obj.uk2.data(), sizeof(obj.uk2));

    obj.data["uk_loads"] = read_array_content(file, all_strs, none_idx, declare, read_string, obj);

    try {
        read_zero(file);
    } catch (ParseException&) {
        obj.fully_parsed = false;
        return file;
    }

    file.read((char*)&uk_int, sizeof(uk_int));
    if (uk_int != (header_size-4)) {
        LOG("Expected to read the pointer to the end of the header 0x%x but found 0x%x", header_size-4, uk_int);
        obj.fully_parsed = false;
        return file;
    }
    file.read((char*)&uk_int, sizeof(uk_int));
    // check that uk_int points to the end of the file
    {
        long long fpos = file.tellg();
        file.seekg (0, std::istream::end);
        long long length = file.tellg();
        if (uk_int != (length-4)) {
            LOG("Expected to read the pointer to the footer 0x%x but found 0x%x", length-4, uk_int);
            obj.fully_parsed = false;
            return file;
        }
        file.seekg (uk_int);
        file.read((char*)obj.footer.data(), sizeof(obj.footer));

        file.seekg (fpos);
    }

    try {
        read_zeros(file, 0xC);
    } catch (ParseException&) {
        obj.fully_parsed = false;
        return file;
    }

    return file;
}
