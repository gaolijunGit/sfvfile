//
// Created by WydD on 01/04/2018.
//


#include <cstring>
#include <sfvfile/util.h>
#include "util/ioutil.h"
#include "sfvfile/bcheader.h"

void check_header(BCHeader &header, const std::string &type) {
    if (memcmp(&type[0], header.filetype, 4) != 0) {
        throw ParseError("header is not a " + type);
    }
}
