//
// Created by WydD on 01/04/2018.
//


#include <istream>
#include <memory>
#include <vector>

std::vector<uint8_t> read_uasset(std::istream &is) {
    is.seekg(0x18);

    uint32_t header_size, file_size;
    is.read((char*)(&header_size), sizeof(uint32_t));
    is.seekg(header_size + 0x20);
    is.read((char*)(&file_size), sizeof(uint32_t));

    std::vector<uint8_t> content(file_size);
    is.read((char*)(content.data()), file_size);
    return content;
}
