//
// Created by WydD on 01/04/2018.
//

#ifndef SFVSIM_NATIVE_IOUTIL_H
#define SFVSIM_NATIVE_IOUTIL_H


#include <memory>
#include <istream>
#include <vector>

/**
 * Read a uasset stream and extract its body
 *
 * @param is the stream (surely from a file)
 * @return the body vector
 */
std::vector<uint8_t> read_uasset(std::istream &is);

/**
 * Cast an entry at the pointer and increment its value by the size of the type
 *
 * @tparam T The type to read (for instance a header)
 * @param ptr the pointer reference
 * @return the structure
 */
template<typename T>
inline T* read_and_advance(uint8_t *&ptr) {
    uint8_t* to_read = ptr;
    ptr += sizeof(T);
    return (T*)(to_read);
}

/**
 * iterator for relative_iterable
 * @tparam T the type to iterate
 */
template<typename T>
class relative_it {
private:
    uint8_t *base;
    uint32_t *ptr;
public:
    explicit relative_it(uint8_t *base, uint32_t *ptr) : base(base), ptr(ptr) {}

    T* operator*() {
        auto offset = *ptr;
        if (offset == 0) {
            return nullptr;
        }
        return (T*)(base + offset);
    }

    bool operator==(const relative_it &other) const { return ptr == other.ptr; }

    bool operator!=(const relative_it &other) const { return !(*this == other); }

    relative_it &operator++() {
        ++ptr;
        return *this;
    }
};

/**
 * represent a list of elements stored as a list of relative pointers
 * pointers are stored relative to the base.
 *
 * example:
 *
 * 0x40: 2      - list size
 * 0x44: 0x60   - list location
 *
 * 0x60: [0x90, 0xA0] - pointer list
 *
 * 0x90: T data1
 * 0xA0: T data2
 *
 * @tparam T a type to interpret
 */
template<typename T>
class relative_iterable {
public:
    relative_it<T> begin() { return relative_it<T>(_base, _ptr); };

    relative_it<T> end() { return relative_it<T>(_base, _ptr + _size); };

    T* operator[](unsigned int i) const {
        if (i >= _size) {
            throw std::domain_error("index outside the possible range");
        }
        auto offset = *(_ptr+i);
        if (offset == 0) {
            return nullptr;
        }
        return (T*)(_base + offset);
    }

    uint32_t size() { return _size; }
    /**
     * construct a relative list view
     *
     * @param base the base of the relative
     * @param size_ptr where the size is stored
     * @param list_ptr where the pointer to the list is stored
     */
    relative_iterable(uint8_t *base, uint8_t *size_ptr, uint8_t *list_ptr) :
            _base(base),
            // resolve the actual pointer of the list
            _ptr((uint32_t *)(base + *(uint32_t *)(list_ptr))),
            // resolve the total size
            _size(*(uint16_t *)(size_ptr)) {}

    relative_iterable(uint8_t *base, uint32_t ptr, uint32_t size) :
            _base(base),
            // resolve the actual pointer of the list
            _ptr((uint32_t*)(base + ptr)),
            // resolve the total size
            _size(static_cast<uint16_t>(size)) {}


private:
    uint8_t *_base;
    uint32_t *_ptr;
    uint16_t _size;
};

#endif //SFVSIM_NATIVE_IOUTIL_H
