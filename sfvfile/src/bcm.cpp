//
// Created by WydD on 01/04/2018.
//

#include "sfvfile/bcm.h"
#include <map>
#include <chrono>
#include "util/ioutil.h"

void load_charges(BCM &obj, uint8_t *base, uint8_t *ptr, uint8_t *list_ptr) {
    struct charge {
        uint16_t direction;
        uint16_t properties;
        uint16_t uk2;
        uint16_t uk3;
        uint16_t frames;
        uint16_t flags;
        uint32_t index;
    };
    auto charges = relative_iterable<charge>(base, ptr, list_ptr);
    obj.charges.reserve(charges.size());
    LOG("Charges %d\n", charges.size());
    int i = 0;
    for (auto it : charges) {
        obj.charges.emplace_back(
                it->direction, it->properties, it->frames, it->flags, i++, it->uk2, it->uk3
        );
    }
}

void load_inputs(BCM &obj, uint8_t *base, uint8_t *ptr, uint8_t *list_ptr) {
    struct input_part {
        uint16_t type;
        uint16_t buffer;
        uint16_t direction;
        uint16_t properties;
        uint16_t uk2;
        uint16_t uk3;
        uint16_t uk4;
        uint16_t uk5;
    };
    struct input_entry {
        uint32_t size;
        input_part parts[16];
    };

    auto inputs = relative_iterable<uint8_t>(base, ptr, list_ptr);
    obj.inputs.reserve(inputs.size());
    LOG("Inputs %d\n", inputs.size());
    for (auto it : inputs) {
        std::vector<std::vector<InputPart>> &input = obj.inputs.emplace_back();
        // nested list, pointers are relative to the current object
        for (auto entry : relative_iterable<input_entry>(it, (uint32_t) 0, 4)) {
            if (entry != nullptr) {
                LOG("InputEntry! parts %d\n", entry->size);
                std::vector<InputPart> &stored_entry = input.emplace_back();
                stored_entry.reserve(entry->size);
                for (uint32_t i = 0; i < entry->size; ++i) {
                    auto &ref = entry->parts[i];
                    stored_entry.emplace_back(
                            ref.type, ref.buffer, ref.direction, ref.properties, ref.uk2, ref.uk3, ref.uk4, ref.uk5
                    );
                }
            } else {
                LOG("Empty InputEntry");
                input.emplace_back();
            }
        }
    }
}

struct move_struct1 {
    uint16_t input;
    uint16_t input_flags;
    uint32_t position_restriction;
    uint32_t unknown3;
    float restriction_distance;
};

struct move_bloody_garden { uint32_t bloody_garden; };

struct move_struct2 {
    uint8_t projectile_limit;
    uint8_t projectile_group;
    uint8_t specific_vtrigger_selected;
    uint8_t specific_vtrigger_needed;
    uint8_t category;
    uint8_t subcategories;
    uint16_t unknown7;
    uint16_t stance;
    uint16_t unknown9;

    uint16_t ex_requires;
    uint16_t ex_consumes;
    uint16_t unknown10;
    uint16_t unknown11;
    uint16_t vmeter_requires;
    uint16_t vmeter_consumes;
    uint16_t vtrigger_activated;
    uint16_t vtrigger_consumes;
    uint16_t input_motion_index;
    int16_t script_index;
    uint32_t unknown17;
    uint32_t unknown18;
};

struct move_unknown19 { uint32_t unknown19; };

struct move_struct3 {
    float unknown20;
    float unknown21;
    uint32_t unknown22;
    uint32_t unknown23;
    uint32_t unknown24;
    uint32_t unknown25;
    uint16_t unknown26;
    uint16_t unknown27;
    uint32_t unknown28;
};
struct move_ver0: move_struct1, move_struct2, move_unknown19, move_struct3 {
    static const uint32_t bloody_garden;
};
const uint32_t move_ver0::bloody_garden = 0;
struct move_ver1: move_struct1, move_bloody_garden, move_struct2, move_struct3 {
    static const uint32_t unknown19;
};
const uint32_t move_ver1::unknown19 = 0;

template<typename T>
inline void emplace_move(std::vector<Move> &moves, void* ptr, uint32_t index, char* name) {
    moves.emplace_back(index, name, *((T*)ptr));
}

void load_moves(BCM &obj, uint8_t *base, uint8_t *size_ptr, uint8_t *list_ptr, uint8_t *names_ptr) {
    auto names = relative_iterable<char>(base, size_ptr, names_ptr);
    auto moves = relative_iterable<uint8_t>(base, size_ptr, list_ptr);
    obj.moves.reserve(moves.size());
    LOG("Moves %d\n", moves.size());
    auto bcm_ver = obj.header.version;
    unsigned int i = 0;
    for (auto it : moves) {
        LOG("Move %s\n", names[i]);
        if (bcm_ver == 0)
            emplace_move<move_ver0>(obj.moves, it, i, names[i]);
        else
            emplace_move<move_ver1>(obj.moves, it, i, names[i]);
        ++i;
    }
}

void load_cancels(BCM &obj, uint8_t *base, uint8_t *size_ptr, uint8_t *list_ptr) {
    struct cancel_list_base {
        uint32_t unknown1;
        uint32_t moves_in_list;
        uint32_t last_index;
        uint32_t moves_ptr;
        uint32_t cancel_int_ptr;
        uint32_t cancel_bytes_ptr;
    };
    auto cancel_lists = relative_iterable<cancel_list_base>(base, size_ptr, list_ptr);
    obj.cancel_lists.reserve(cancel_lists.size());
    LOG("Cancel Lists %d\n", cancel_lists.size());
    int i = 0;
    for (auto cl : cancel_lists) {
        if (cl == nullptr) {
            obj.cancel_lists.emplace_back();
        } else {
            LOG("Cancel List #%d with %d moves\n", i, cl->moves_in_list);

            obj.cancel_lists.push_back(std::make_unique<CancelList>(cl->unknown1, cl->moves_in_list));
            auto &cancel_list = *obj.cancel_lists[i];

            // move ids is a int16[moves_in_list]
            auto move_ids = (uint16_t*)((uint8_t *) cl + cl->moves_ptr);
            // uk bytes array is a relative list byte[36][last_index]
            auto uk_bytes_array = relative_iterable<std::array<uint8_t, 36>>((uint8_t*)cl, cl->cancel_bytes_ptr, cl->last_index);
            // uk ints are optional int32[2][moves_in_list]
            auto uk_ints = cl->cancel_int_ptr > 0 ? (std::array<uint32_t, 2> *)((uint8_t *) cl + cl->cancel_int_ptr) : nullptr;
            for(uint16_t n = 0 ; n < cl->moves_in_list ; ++n) {
                auto move_id = move_ids[n];
                cancel_list.moves.emplace_back(move_id, *uk_bytes_array[move_id], uk_ints == nullptr ? std::unique_ptr<std::array<uint32_t, 2>>() : std::make_unique<std::array<uint32_t, 2>>(uk_ints[n]));
            }
        }
        i++;
    }
}

std::istream &operator>>(std::istream &is, BCM &obj) {
    // load the uasset file
    std::vector<uint8_t> content = read_uasset(is);
    uint8_t *base = content.data();
    obj.load_data(base);
    return is;
}


void BCM::load_data(uint8_t *base) {
    uint8_t *ptr = base;

    auto t = std::chrono::system_clock::now();

    // read the bch header
    header = *read_and_advance<BCHeader>(ptr);
    check_header(header, "#BCM");

    load_charges(*this, base, ptr, ptr + 8);
    load_inputs(*this, base, ptr + 2, ptr + 12);
    load_moves(*this, base, ptr + 4, ptr + 16, ptr + 20);
    load_cancels(*this, base, ptr + 6, ptr + 24);

    auto t2 = std::chrono::system_clock::now();

    std::chrono::duration<double> diff = t2-t;

    LOG("Loading BCM took %fms\n", diff.count()*1000);

}
