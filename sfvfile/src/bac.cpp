//
// Created by WydD on 07/04/2018.
//

#include <chrono>
#include <string>
#include <iostream>
#include <util/ioutil.h>
#include "sfvfile/bac.h"

#include "loaders/timeline.h"
#include "loaders/auto_cancel.h"
#include "loaders/status.h"
#include "loaders/force.h"
#include "loaders/cancel.h"
#include "loaders/command.h"
#include "loaders/hitbox.h"
#include "loaders/hurtbox.h"
#include "loaders/pushbox.h"
#include "loaders/position.h"
#include "loaders/hitbox_effect.h"

tick_flags null_flags{0, 0, 0, 0};

template<typename T, typename H, typename F>
void load_type_content(Script &script, H *type_header, F &apply) {
    const auto size = type_header->count;
    const auto flagOffset = type_header->flagOffset;
    auto ticks = (tick_header*)(((uint8_t*)type_header) + type_header->tickOffset);
    auto flags = type_header->flagOffset > 0 ? (tick_flags*)(((uint8_t*)type_header) + flagOffset) : nullptr;
    auto * ref = (T*) (((uint8_t*)type_header)+type_header->dataOffset);
    for (size_t i = 0 ; i < size ; ++i) {
        auto &elt = ref[i];
        auto &tick = ticks[i];
        if (static_cast<void*>(&tick) == static_cast<void*>(ref)) {
            // avoid bad count value (see BAC_VEG script 1990)
            break;
        }
        if (flags != nullptr) {
            auto &tf = flags[i];
            apply(script, tick, tf, elt);
        } else {
            apply(script, tick, null_flags, elt);
        }
    }
}

enum type_enum: int16_t {
    AUTO_CANCELS, STATUS, FORCES, CANCELS, COMMANDS, HITBOXES, HURTBOXES, PUSHBOXES,
    ANIMATIONS, TYPE9, SOUND_EFFECTS, VISUAL_EFFECTS,
    POSITIONS
};
struct script_type_header_base {
    type_enum type;
    uint16_t count;
    uint32_t tickOffset;
    uint32_t dataOffset;
};
struct script_type_header_ver1: script_type_header_base {
    uint32_t flagOffset;
};
struct script_type_header_ver0: script_type_header_base {
    static const uint32_t flagOffset;
};

const uint32_t script_type_header_ver0::flagOffset = 0;

template <int version, typename T>
void load_script_types(Script &script, T* base) {
    typedef typename std::conditional<version == 1, script_type_header_ver1, script_type_header_ver0>::type script_type_header;

    uint32_t number_of_types = base->number_of_types;
    uint32_t types_ptr = base->types_ptr;
    auto * types = (script_type_header*) (((uint8_t*)base)+types_ptr);
    for (size_t i = 0 ; i < number_of_types ; ++i) {
        script_type_header *type_header = &types[i];
        LOG("Type %d -> #%d\n", type_header->type, type_header->count)
        switch (type_header->type) {
            case AUTO_CANCELS:
                typedef typename std::conditional<version == 1, auto_cancel_ver1, auto_cancel_ver0>::type auto_cancel_ver;
                load_type_content<auto_cancel_ver>(script, type_header, load_auto_cancel<auto_cancel_ver>);
                break;
            case STATUS:
                load_type_content<status_struct>(script, type_header, load_status);
                break;
            case FORCES:
                load_type_content<force_struct>(script, type_header, load_force);
                break;
            case CANCELS:
                load_type_content<cancel_struct>(script, type_header, load_cancel);
                break;
            case COMMANDS:
                load_type_content<command_struct>(script, type_header, load_command);
                break;
            case HITBOXES:
                typedef typename std::conditional<version == 1, hitbox_ver1, hitbox_ver0>::type hitbox_ver;
                load_type_content<hitbox_ver>(script, type_header, load_hitbox<hitbox_ver>);
                break;
            case HURTBOXES:
                typedef typename std::conditional<version == 1, hurtbox_ver1, hurtbox_ver0>::type hurtbox_ver;
                load_type_content<hurtbox_ver>(script, type_header, load_hurtbox<hurtbox_ver>);
                break;
            case PUSHBOXES:
                load_type_content<pushbox_base>(script, type_header, load_pushbox);
                break;
            case POSITIONS:
                load_type_content<position_base>(script, type_header, load_position);
                optimize_positions(script.positions);
                break;
            case ANIMATIONS:
            case TYPE9:
            case SOUND_EFFECTS:
            case VISUAL_EFFECTS:
                // ignoring non-gameplay stuff
                break;
            default:
                throw ParseError("Unknown script type " + std::to_string(type_header->type));
        }
    }
}

void load_scripts(BAC &obj, uint8_t* table, uint16_t count, uint32_t names_ptr, uint32_t ptr, int16_t uk1) {
    struct script_header {
        int32_t first_hitbox_frame;
        int32_t last_action_frame;
        int32_t interrupt_frame;
        uint32_t total_ticks;

        uint32_t execution_flags;
        float slide_vx;
        float slide_vy;
        float slide_vz;
        float slide_ax;
        float slide_ay;
        float slide_az;
        uint32_t flags;
        int32_t type_offset;
        uint32_t number_of_types;
        uint32_t unk13;
        uint32_t types_ptr;
    };
    struct eff_script_header {
        int16_t unknown12;
        int16_t unknown13;
        int16_t unknown14;
        int16_t unknown15;
        int16_t unknown16;
        int16_t unknown17;
        float unknown18;
        int16_t unknown19;
        int16_t unknown20;
        int16_t unknown21;
        int16_t unknown22;
    };
    auto names = relative_iterable<char>(table, names_ptr, count);
    auto scripts = relative_iterable<script_header>(table, ptr, count);
    auto &script_table = obj.script_tables.emplace_back(uk1);
    LOG("Scripts %d\n", count);
    script_table.scripts.reserve(count);
    int i = 0;
    for (auto script : scripts) {
        if (script == nullptr) {
//            LOG("[%d] Empty Script\n", i);
            script_table.scripts.emplace_back();
            ++i;
            continue;
        }
        eff_script_header* eff_header = nullptr;
        if (script->types_ptr != sizeof(script_header)) {
            if (script->types_ptr != sizeof(script_header)+sizeof(eff_script_header)) {
                throw ParseError("Script header is neither a script header or an eff script header: actual size "+std::to_string(script->types_ptr));
            }
            eff_header = (eff_script_header*)(((uint8_t*)script) + sizeof(script_header));
        }
        LOG("[%d] Loading script %s\n", i, names[i]);
        script_table.scripts.push_back(std::make_unique<Script>(
                i, names[i], script->first_hitbox_frame, script->last_action_frame, script->interrupt_frame,
                script->total_ticks, script->execution_flags, script->slide_vx, script->slide_vy, script->slide_vz,
                script->slide_ax, script->slide_ay, script->slide_az, script->flags, script->type_offset, script->unk13,
                eff_header == nullptr ? nullptr : std::make_unique<EffectHeader>(
                        eff_header->unknown12, eff_header->unknown13, eff_header->unknown14, eff_header->unknown15,
                        eff_header->unknown16, eff_header->unknown17, eff_header->unknown18, eff_header->unknown19,
                        eff_header->unknown20, eff_header->unknown21, eff_header->unknown22
                )
        ));
        if (obj.header.version == 1)
            load_script_types<1>(*script_table.scripts[i], script);
        else
            load_script_types<0>(*script_table.scripts[i], script);
        ++i;
    }
}

void load_script_tables(BAC &obj, uint8_t *base, uint8_t *size_ptr, uint8_t *list_ptr) {
    struct script_table {
        int16_t unknown1;
        uint16_t move_count;
        uint32_t moves;
        uint32_t names;
    };

    auto script_tables = relative_iterable<script_table>(base, size_ptr, list_ptr);
    obj.script_tables.reserve(script_tables.size());
    LOG("Script Tables %d\n", script_tables.size());
    for (auto table : script_tables) {
        load_scripts(obj, (uint8_t*) table, table->move_count, table->names, table->moves, table->unknown1);
    }
}

void load_hitbox_effects(BAC &obj, uint8_t* base, uint8_t* size_ptr, uint8_t* list_ptr) {
    auto hitbox_effects = relative_iterable<uint8_t>(base, size_ptr, list_ptr);
    obj.hitbox_effects.reserve(hitbox_effects.size());
    LOG("Hitbox Effects count %d\n", hitbox_effects.size());
    int i = 0;
    for (auto he : hitbox_effects) {
        auto &final = obj.hitbox_effects.emplace_back();
        if (he == nullptr) {
            ++i;
            continue;
        }
        auto situation = relative_iterable<hitbox_effect_base>(he, static_cast<uint32_t>(0), 20);
        final.reserve(20);
        for (auto he_struct : situation) {
            final.emplace_back(*he_struct);
        }
        LOG("Hitbox Effects %d is available: Stand(%d,%d), Counterhit(%d,%d), Guard(%d,%d)\n", i,
            final[HIT_STAND].type, final[HIT_STAND].damage, final[COUNTERHIT_STAND].type,
            final[COUNTERHIT_STAND].damage, final[GUARD_STAND].type, final[GUARD_STAND].damage);
        ++i;
    }
}

std::istream &operator>>(std::istream &is, BAC &obj) {
    // load the uasset file
    std::vector<uint8_t> content = read_uasset(is);
    uint8_t* base = content.data();
    obj.load_data(base);
    return is;
}

void BAC::load_data(uint8_t *base) {
    uint8_t* ptr = base;
    auto t = std::chrono::system_clock::now();
    // read the bch header
    header = *read_and_advance<BCHeader>(ptr);
    check_header(header, "#BAC");

    load_script_tables(*this, base, ptr, ptr + 4);
    load_hitbox_effects(*this, base, ptr + 2, ptr + 8);

    auto t2 = std::chrono::system_clock::now();

    std::chrono::duration<double> diff = t2 - t;

    LOG("Loading BAC took %fms\n", diff.count()*1000);

}
