//
// Created by WydD on 28/05/2018.
//

#include "bac_binding.h"


int init_bac(PyBAC *self, PyObject *args, PyObject *kwds) {
    PyObject* object;
    if (!PyArg_ParseTuple(args, "O", &object)) {
        return -1;
    };

    auto *const bac = load_file<BAC>(object);
    self->bac = bac;

    self->script_tables = convertContainerToList(
            self->bac->script_tables,
            [](auto &script) {
                return rawObjAsPythonType(script, &PyBACScriptTableType);
            }
    );

    self->hitbox_effects = convertContainerToList(
            self->bac->hitbox_effects,
            [](auto &hitbox_effects) {
                return convertContainerToList(hitbox_effects, [](auto &he) {
                    return rawObjAsPythonType(he, &PyBACHitboxEffectType);
                });
            }
    );

    return 0;
}

static PyGetSetDef PyBAC_getset[] = {
        PyDirectGetterDef(PyBAC, script_tables, list[sfvfile_exp.ScriptTable]),
        PyDirectGetterDef(PyBAC, hitbox_effects, list[list[sfvfile_exp.HitboxEffect]]),
        {nullptr} /* Sentinel */
};

PyTypeObject PyBACType = {
        TypeDef_HEAD(BAC, PyBAC),
        [](PyObject *_self) {
            auto self = (PyBAC *) _self;
            Py_XDECREF(self->script_tables);
            Py_XDECREF(self->hitbox_effects);
            delete self->bac;
            genericFree(_self);
        },
        TypeDef(
                nullptr,
                PyBAC_getset,
                init_bac
        )
};

