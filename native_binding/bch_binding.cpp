//
// Created by WydD on 29/05/2018.
//

#include "bch_binding.h"

int init_bch(PyBCH *self, PyObject *args, PyObject *kwds) {
    PyObject* object;
    if (!PyArg_ParseTuple(args, "O", &object)) {
        return -1;
    };
    self->ref = load_file<BCH>(object);
    return 0;
}

static PyGetSetDef PyBCH_getset[] = {
        PyGetterDef(PyBCH, life, PyLong_FromLong, int),
        PyGetterDef(PyBCH, stun, PyLong_FromLong, int),
        PyGetterDef(PyBCH, vgauge, PyLong_FromLong, int),
        PyGetterDef(PyBCH, vgauge_2, PyLong_FromLong, int),
        PyGetterDef(PyBCH, vtrigger, PyLong_FromLong, int),
        PyGetterDef(PyBCH, vtrigger_2, PyLong_FromLong, int),
        PyGetterDef(PyBCH, claw_endurance, PyLong_FromLong, int),
        PyGetterDef(PyBCH, mask_endurance, PyLong_FromLong, int),
        PyGetterDef(PyBCH, vtrigger_move_speed, PyLong_FromLong, int),
        {"content", [](PyObject *_self, void *) {
            auto self = (PyBCH*)_self;
            auto dict = PyDict_New();
            for (auto &it : self->ref->content) {
                auto str = it.first;
                PyObject *item = PyLong_FromLong(it.second);
                PyDict_SetItemString(dict, str.c_str(), item);
                Py_DECREF(item);
            }
            return dict;
        }, nullptr, ":type: dict"},
        PySentinel
};

PyTypeObject PyBCHType = {
        TypeDef_HEAD(CancelList, PyBCH),
        [](PyObject* _self) {
            auto self = (PyBCH*)_self;
            delete self->ref;
            genericFree(_self);
        },
        TypeDef(
                nullptr,
                PyBCH_getset,
                init_bch
        )
};
