//
// Created by WydD on 21/05/2018.
//

#include "binding_util.h"
#include "bac_hitbox_effect.h"

static PyGetSetDef PyBACHitboxEffect_getset[] = {
        PyGetterDef(PyBACHitboxEffect, type, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, damage_type, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, damage, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, stun, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, vtimer, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, ex_attacker, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, ex_defender, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, vgauge, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, hit_freeze_attacker, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, hit_freeze_defender, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, fuzzy_effect, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, main_recovery_duration, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, knockdown_duration, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, juggle_start, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, damage_scaling_group, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, knock_back, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACHitboxEffect, fall_speed, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACHitboxEffect, unused1, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, unused2, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index9, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index14, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index18, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index22, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index23, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index24, PyLong_FromLong, int),
        PyGetterDef(PyBACHitboxEffect, index25, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACHitboxEffectType = {
        TypeDef_HEAD(HitboxEffect, PyBACHitboxEffect),
        [](PyObject *_self) {
            auto self = (PyBACHitboxEffect *) _self;
            Py_TYPE(self)->tp_free(self);
        },
        TypeDef(
                nullptr,
                PyBACHitboxEffect_getset,
                genericInit
        )
};

