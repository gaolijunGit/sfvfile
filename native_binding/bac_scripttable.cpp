//
// Created by WydD on 21/05/2018.
//

#include <Python.h>
#include <sfvfile/bac.h>

#include "binding_util.h"
#include "bac_script.h"
#include "bac_scripttable.h"

int PyBACScriptTable_init(PyBACScriptTable *self, PyObject *args, PyObject *kwds) {
    self->scripts = convertContainerToList(
            self->ref->scripts,
            [](auto &script) { return ptrAsPythonType(script, &PyBACScriptType); }
    );
    return 0;
}

//(getter)[](PyObject* self) { return PyLong_FromLong(((PyBACScriptTable*)self)->script_table->unknown1); }
static PyGetSetDef PyBACScriptTable_getset[] = {
        PyGetterDef(PyBACScriptTable, unknown1, PyLong_FromLong, int),
        PyDirectGetterDef(PyBACScriptTable, scripts, list[sfvfile_exp.Script]),
        PySentinel
};

PyTypeObject PyBACScriptTableType = {
        TypeDef_HEAD(ScriptTable, PyBACScriptTable),
        [](PyObject *_self) {
            auto self = (PyBACScriptTable *) _self;
            Py_XDECREF(self->scripts);
            Py_TYPE(self)->tp_free(self);
        },
        TypeDef(
                nullptr,
                PyBACScriptTable_getset,
                PyBACScriptTable_init
        )
};

